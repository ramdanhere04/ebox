---
description: EVM-Based project environment cli-helpers
---

## Ebox
[![build](https://circleci.com/gh/penta-expo/ebox.svg?style=svg)](https://circleci.com/gh/penta-expo/ebox)
[![codecov](https://codecov.io/gh/penta-expo/ebox/branch/master/graph/badge.svg?token=mcoPeirwbV)](https://codecov.io/gh/penta-expo/ebox)
